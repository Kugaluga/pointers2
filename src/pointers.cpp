#include <iostream>

using namespace std;


int main()
{
  int x = 25, y = 2 * x;    
  int *a = &x, *b = a;
  int c = *a, d = 2 * *b;

  cout << "a = "<< *a <<", address of a = "<< &a <<endl;
  cout << "b = "<< *b <<", address of b = "<< &b <<endl;
  cout << "c = "<< c <<", address of c = "<< &c <<endl;
  cout << "d = "<< d <<", address of d = "<< &d <<endl;
  cout << "x = "<< x <<", address of x = "<< &x <<endl;
  cout << "y = "<< y <<", address of y = "<< &y <<endl;

 



}
